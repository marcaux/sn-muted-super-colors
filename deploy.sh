#!/bin/sh
apt update
apt install zip
zip -r sn-muted-super-colors.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-muted-super-colors.zip public/
cd public
unzip sn-muted-super-colors.zip
